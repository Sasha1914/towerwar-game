// game characters 
var enemies = [];
var projectiles = [];
var systems = [];
var towers = [];
var newEnemies = [];
var newProjectiles = [];
var newTowers = [];

// map 
var initMap=JSON.parse(LZString.decompressFromBase64('N4IgJglgzgDgNgQwJ4gFwG10gOYCcFRQgA0O+hJZBRpe1ldFt5NVTbrjnLDPz9/dl14COIoXzGDuo4dPFEAusSxyQUCGACmAdwRwA1pQ3a9h45t36j821PtrHkp6JNXzpN2ZvrL3i6bWAe4+XkGefuG+gR7RIcH+8sqqkmGxuAD2CGAJUS4Sss5FhSUF7Gk+uADCGbgAdlq4lJnZzVk5pC0dIF1trZ3tzTX1jbmxwsn2FX3d03b5MmWLywqrETGVg+vx24lxe3P7UYe9u3k8k2onW0fjxUtrDvcrdtf9tz4Lj1eR6TeHExUU1+m3eAOe3wh8yhwI2MzGnxhXzOfzBINWl1S6IGaLh0NKLyeBMhrmxPX+ZORRPKZNOHwxQJ+ePJuJ21MJVM5MLes0pSO5tIpzMBKVJzLph0lZKlzJlbLlB2lgtZitlSrVGrZEr59ExYq1wwaTRx3TpZpu5veltNFttVrtNvtTsdLqGtSNCIZoppmtV8vV/t9xwDftDwaDqN5EdCIfDgfjYbuusZ/OJ+Ie6Y5qZ9WqFbK5afZJIoeozRczxazhapPM9Ffr5aLparZYLrezLdYtZRiOrHcrXsbbc7A4bNeVUfz/bH/ebo6H04Xff1e21wsXw4Hc57dfpO/3e8PCrjiZj0fhB+PsSvZ4TJ/Oye9XbJcENoxNF5Zro/P6/n+t/4OoBzrAd+h4irCuYqve16xrB56XnBt6np+N67mhiEIWI26HmuU7Lu2BEjgeeF7JuM5ESAOHduBG50ZRJF5mR9GEaxSgpiuUSkQ+bHzuRkGrkxPHEUuvHUROu78VJi40eCDGiSO4nikJSa8RRakCVxKm9hp0nEkpUGTihmF3vBpnITBFlmYJ0HWZZ6FIQ566Pppkage5v4AZ5QHeSBvlgV5PRvsatEuUyhlOeZkXGUejkmTZRn2fFSW4dpg7hQlkksSJ/GyTqulYsptk6exT67txqk5dl855c5BWcW5B4QRlWnFXx1Xqc+RWJZV7UNaCPUlVRHE5plTUdQpNUSeN8ktY1oUliNXURcldlrVZG3RSlGF/jN9Wja1g3pYVbKvu6767ZdgXXT5V23WdIwhXJ+3LWNC2la5m2rV9sVYTt/1xe9fUHetWVhRNekvcDVWzRDFzKOA0DwMgAAiEBNBg6AAAzEDjeO4wT+NE4TJPE2ThPJOTVOkzT1Nk5TBMAMy0yzxPMwATMQnPc1zvOc3jDM48zdOs8QwuiwAjBTKj4+LIvk3LLNSwLMtM6LIuK+Tyu44LYvq6Lms09rWO64b8tqyLxum/r1NmyTVuqzjAAsxAu27rse+7Xse87nt+y7DvYzb+t21Tuvm7bwcq0HEcG1HOuO/HCtJ+HSck6HtOp7HbMp4n2c0xn9OJ97Jf+37vul+70f48baeF6Tuu1/n9dE43aey7nMfEE38ct9LXc97HffVwTg/B8PCcD937d69nqc8wvfNLzzOOL2v/c19P+dC/rgeb9vFuSxvo8zzvlvHzjY/m/Xe8nwfZ9HyPl+n7Pj+T5v6+f8vu8Xy/99Z3/KOigEZ4E0GgTAzNIF6ygTA6BcDYEIPgWLZIiDUFILQRg5BKgoGc0wWgr+i9IEoL1rg9BZCcHkJIdA4hHNKGYNIfg6h2CqF4LIQwthTD0AUNYYg9hvDOHcLoRw+hAiWFCP4SIohzDaEEO/rI9eUiuHiKEXwuhNDlGMJ4VgpRWjNEaPUbopBqiMEGI0UYsxjNTGGMETwqxFixG2OkXI5x8iXGiNodYhx4j1HGOUb41BPj7E2O8U4/xKiLGBM8R4/RoSgleLwZEuJYSEGBNcWk5xijgmGOSbzTJ8SzE5IYYkzxhT3G5KSeEvJ0SSmVO0VkgptTLGxPSQoyhRSnFRLiU0nRXSunANIAAWy0AAF2yAgUZ4D0B1AAK5wDgMQGZcyFmzPmYs1ZKzllLLWZs9ZWyNnbIOfso5eyTm7LOTsi5hzTnJCuec25lzjl3MeQ805LynmvPuZ8lZNznlfI+b8gF/ygXvJBW8sFayfnAvBYC0FfzYUwuhVCiFKg4WIvhUihFqKsWYu+SinFGKCXoqJWikltzIXEuxYS0l+KKUwvJdSqllLaWMppTs+lTKGXMq5ZytF7LWUcoFfyoVwK+UsrFdywV4q2V4qlZKiVwr5VzNFYqlVPK5W8plaq9V2qFUaqmbqnVsqDU0uVWq41RqLUgtNYarV5qVXWrtWay1TqzkOudTal1Lq3W2vdY6pl3rPUeqDSazVga/XhstQG4NvqY1WtDdGn1ibOVRojUmhN0r9WxrDVmuVKac2puza6+NBb02lseXmtNJbU0VsLbWstSri35qbZW65jaW3trreWttnbm09qLZmjt9bB24oHX2sdQ62UIxgOMgAFkQTG99AEmzzkuoBicJ451cU/V+B8b6/w3enH+26D0d3Psel+e7z2Lsve/Q+u6j23ormXSu5dn1+1vou09a6u6fp3TbAB9654ruvZ3X9X6I4AbrqB1dG7dYvvgz7N9Vdf5XxDtB5+IGgNT0w9+/eM9YOJ1QxrdDW98MkaI3HLDH83Ff1XjRlDF6H3Luw2Rs9j7SOAbfsxvDnHqYfo41Btj3G76sa423Fp38hOQbA+raTq6w4Iy0AADwgMM8BEsADsXMJYI1gAgHQdQYAZAgHUYZ87MDcxQdzHTCMABG2BwGt1ILZ2o2gMbswAKweec65xoABBOAMAZ0IDQIzKWIAADGGQ4DzsZt58kOh53sydgAXyAA=='));
var cols;
var rows;
var tileZoom = 2;
var ts = 24;            // tile size
var zoomDefault = ts;

var particleAmt = 32;   // number of particles to draw per explosion

var tempSpawnCount = 40;

var custom;             // custom map JSON
var display;            // graphical display tiles
var displayDir;         // direction display tiles are facing
                        // (0 = none, 1 = left, 2 = up, 3 = right, 4 = down)
var dists;              // distance to exit
var grid;               // tile type
                        // (0 = empty, 1 = wall, 2 = path, 3 = tower,
                        //  4 = enemy-only pathing)
var metadata;           // tile metadata
var paths;              // direction to reach exit
var visitMap;           // whether exit can be reached
var walkMap;            // walkability map

var exit;
var spawnpoints = [];
var tempSpawns = [];

var cash;
var health;
var maxHealth;
var wave;

var spawnCool;          // number of ticks between spawning enemies

var bg;                 // background color
var border;             // color to draw on tile borders
var borderAlpha;        // alpha of tile borders

var selected;
var towerType;

var sounds;             // dict of all sounds
var boomSound;          // explosion sound effect

var healthBar = true;   // display enemy health bar
var paused;             // whether to update or not
var scd;                // number of ticks until next spawn cycle
var showEffects = true; // whether or not to display particle effects
var skipToNext = false; // whether or not to immediately start next wave
var stopFiring = false; // whether or not to pause towers firing
var toCooldown;         // flag to reset spawning cooldown
var toPathfind;         // flag to update enemy pathfinding
var toPlace;            // flag to place a tower
var toWait;             // flag to wait before next wave
var wcd;                // number of ticks until next wave

var minDist = 15;       // minimum distance between spawnpoint and exit
var resistance = 0.5;   // percentage of damage blocked by resistance
var sellConst = 0.8;    // ratio of tower cost to sell price
var wallCover = 0.1;    // percentage of map covered by walls
var waveCool = 120;     // number of ticks between waves
var weakness = 0.5;     // damage increase from weakness


// Misc functions

// Spawn a group of enemies, alternating if multiple types
function addGroup(group) {
    var count = group.pop();
    for (var i = 0; i < count; i++) {
        for (var j = 0; j < group.length; j++) {
            newEnemies.push(group[j]);
        }
    }
}

// Prepare a wave
function addWave(pattern) {
    spawnCool = pattern.shift();
    for (var i = 0; i < pattern.length; i++) {
        addGroup(pattern[i]);
    }
}

// Load all sounds
function loadSounds() {
    sounds = {};
    
    // Missile explosion
    sounds.boom = loadSound('sounds/boom.wav');
    sounds.boom.setVolume(0.3);

    // Missile launch
    sounds.missile = loadSound('sounds/missile.wav');
    sounds.missile.setVolume(0.3);

    // Enemy death
    sounds.pop = loadSound('sounds/pop.wav');
    sounds.pop.setVolume(0.4);

    // Railgun
    sounds.railgun = loadSound('sounds/railgun.wav');
    sounds.railgun.setVolume(0.3);

    // Sniper rifle shot
    sounds.sniper = loadSound('sounds/sniper.wav');
    sounds.sniper.setVolume(0.2);

    // Tesla coil
    sounds.spark = loadSound('sounds/spark.wav');
    sounds.spark.setVolume(0.3);

    // Taunt enemy death
    sounds.taunt = loadSound('sounds/taunt.wav');
    sounds.taunt.setVolume(0.3);
}

// Buy and place a tower if player has enough money
function buy(t) {
    if (cash >= t.cost) {
        cash -= t.cost;
        toPlace = false;
        selected = t;
        if (grid[t.gridPos.x][t.gridPos.y] === 0) toPathfind = true;
        updateInfo(t);
        newTowers.push(t);
    }
}

// tile type
// (0 = empty, 1 = wall, 2 = path, 3 = tower,
//  4 = enemy-only pathing)

// Check if all conditions for placing a tower are true
function canPlace(col, row) {
    if (!toPlace) return false;
    var g = grid[col][row];
    if (g === 3) return true;
    if (g === 1 || g === 2 || g === 4) return false;
    if (!empty(col, row) || !placeable(col, row)) return false;
    return true;
}

// Check if spawn cooldown is done and enemies are available to spawn
function canSpawn() {
    return newEnemies.length > 0 && scd === 0;
}

// Clear tower information
function clearInfo() {
    document.getElementById('info-div').style.display = 'none';
}

// Check if all conditions for showing a range are true
function doRange() {
    return mouseInMap() && toPlace && typeof towerType !== 'undefined';
}


// tile type
// (0 = empty, 1 = wall, 2 = path, 3 = tower,
//  4 = enemy-only pathing)

// Check if tile is empty
function empty(col, row) {
    // Check if not walkable
    if (!walkable(col, row)) return false;
    // Check if spawnpoint
    for (var i = 0; i < spawnpoints.length; i++) {
        var s = spawnpoints[i];
        if (s.x === col && s.y === row) return false;
    }
    // Check if exit
    if (typeof exit !== 'undefined') {
        if (exit.x === col && exit.y === row) return false;
    }
    return true;
}

// Find tower at specific tile, otherwise return null
function getTower(col, row) {
    for (var i = 0; i < towers.length; i++) {
        var t = towers[i];
        if (t.gridPos.x === col && t.gridPos.y === row) return t;
    }
    return null;
}

// BFS search  TB deleted  
// Return map of visitability
function getVisitMap(walkMap) {
    var frontier = [];
    var target = vts(exit);
    frontier.push(target);
    var visited = {};
    visited[target] = true;

    // Fill visited for every tile
    while (frontier.length !== 0) {
        var current = frontier.shift();
        var t = stv(current);
        var adj = neighbors(walkMap, t.x, t.y, true);

        for (var i = 0; i < adj.length; i++) {
            var next = adj[i];
            if (!(next in visited)) {
                frontier.push(next);
                visited[next] = true;
            }
        }
    }

    return visited;
}

// Return walkability map
function getWalkMap() { 
    var walkMap = [];
    for (var x = 0; x < cols; x++) {
        walkMap[x] = [];
        for (var y = 0; y < rows; y++) {
            walkMap[x][y] = walkable(x, y);
        }
    }
    return walkMap;
}

// Check if wave is at least min and less than max
function isWave(min, max) {
    if (typeof max === 'undefined') return wave >= min;
    return wave >= min && wave < max;
}

// Load map from template
// Always have an exit and spawnpoints if you do not have a premade grid
function loadMap() {
    health = 40;
    cash = 55;
    // name== loops
    var m = initMap;
    // Grids
    display = copyArray(m.display);
    displayDir = copyArray(m.displayDir);
    grid = copyArray(m.grid);
    metadata = copyArray(m.metadata);
    paths = copyArray(m.paths);
    // Important tiles
    exit = createVector(m.exit[0], m.exit[1]);
    spawnpoints = [];
    for (var i = 0; i < m.spawnpoints.length; i++) {
        var s = m.spawnpoints[i];
        spawnpoints.push(createVector(s[0], s[1]));
    }
    // Colors
    bg = m.bg;
    border = m.border;
    borderAlpha = m.borderAlpha;
    // Misc
    cols = m.cols;
    rows = m.rows;

    resizeFit();
    tempSpawns = [];
    recalculate();
}

// Increment wave counter and prepare wave
function nextWave() {
    addWave(randomWave());
    wave++;
}

// Check if no more enemies
function noMoreEnemies() {
    return enemies.length === 0 && newEnemies.length === 0;
}

function outsideMap(e) {
    return outsideRect(e.pos.x, e.pos.y, 0, 0, width, height);
}

// Toggle pause state
function pause() {
    paused = !paused;
}

// Return false if blocking a tile would invalidate paths to exit
function placeable(col, row) {
    var walkMap = getWalkMap();
    walkMap[col][row] = false;
    var visitMap = getVisitMap(walkMap);

    // Check spawnpoints
    for (var i = 0; i < spawnpoints.length; i++) {
        if (!visitMap[vts(spawnpoints[i])]) return false;
    }

    // Check each enemy
    for (var i = 0; i < enemies.length; i++) {
        var e = enemies[i];
        var p = gridPos(e.pos.x, e.pos.y);
        if (p.equals(col, row)) continue;
        if (!visitMap[vts(p)]) return false;
    }

    return true;
}

// Generate a random wave
function randomWave() {
    var waves = [];

    if (isWave(0, 3)) {
        waves.push([40, ['weak', 50]]);
    }
    if (isWave(2, 4)) {
        waves.push([20, ['weak', 25]]);
    }
    if (isWave(2, 7)) {
        waves.push([30, ['weak', 25], ['strong', 25]]);
        waves.push([20, ['strong', 25]]);
    }
    if (isWave(3, 7)) {
        waves.push([40, ['fast', 25]]);
    }
    if (isWave(4, 14)) {
        waves.push([20, ['fast', 50]]);
    }
    if (isWave(5, 6)) {
        waves.push([20, ['strong', 50], ['fast', 25]]);
    }
    if (isWave(8, 12)) {
        waves.push([20, ['medic', 'strong', 'strong', 25]]);
    }
    if (isWave(10, 13)) {
        waves.push([20, ['medic', 'strong', 'strong', 50]]);
        waves.push([30, ['medic', 'strong', 'strong', 50], ['fast', 50]]);
        waves.push([5, ['fast', 50]]);
    }
    if (isWave(12, 16)) {
        waves.push([20, ['medic', 'strong', 'strong', 50], ['strongFast', 50]]);
        waves.push([10, ['strong', 50], ['strongFast', 50]]);
        waves.push([10, ['medic', 'strongFast', 50]]);
        waves.push([10, ['strong', 25], ['stronger', 25], ['strongFast', 50]]);
        waves.push([10, ['strong', 25], ['medic', 25], ['strongFast', 50]]);
        waves.push([20, ['medic', 'stronger', 'stronger', 50]]);
        waves.push([10, ['medic', 'stronger', 'strong', 50]]);
        waves.push([10, ['medic', 'strong', 50], ['medic', 'strongFast', 50]]);
        waves.push([5, ['strongFast', 100]]);
        waves.push([20, ['stronger', 50]]);
    }
    if (isWave(13, 20)) {
        waves.push([40, ['tank', 'stronger', 'stronger', 'stronger', 10]]);
        waves.push([10, ['medic', 'stronger', 'stronger', 50]]);
        waves.push([40, ['tank', 25]]);
        waves.push([20, ['tank', 'stronger', 'stronger', 50]]);
        waves.push([20, ['tank', 'medic', 50], ['strongFast', 25]]);
    }
    if (isWave(14, 20)) {
        waves.push([20, ['tank', 'stronger', 'stronger', 50]]);
        waves.push([20, ['tank', 'medic', 'medic', 50]]);
        waves.push([20, ['tank', 'medic', 50], ['strongFast', 25]]);
        waves.push([10, ['tank', 50], ['strongFast', 25]]);
        waves.push([10, ['faster', 50]]);
        waves.push([20, ['tank', 50], ['faster', 25]]);
    }
    if (isWave(17, 25)) {
        waves.push([20, ['taunt', 'stronger', 'stronger', 'stronger', 25]]);
        waves.push([20, ['spawner', 'stronger', 'stronger', 'stronger', 25]]);
        waves.push([20, ['taunt', 'tank', 'tank', 'tank', 25]]);
        waves.push([40, ['taunt', 'tank', 'tank', 'tank', 25]]);
    }
    if (isWave(19)) {
        waves.push([20, ['spawner', 1], ['tank', 20], ['stronger', 25]]);
        waves.push([20, ['spawner', 1], ['faster', 25]]);
    }
    if (isWave(23)) {
        waves.push([20, ['taunt', 'medic', 'tank', 25]]);
        waves.push([20, ['spawner', 2], ['taunt', 'medic', 'tank', 25]]);
        waves.push([10, ['spawner', 1], ['faster', 100]]);
        waves.push([5, ['faster', 100]]);
        waves.push([
            20, ['tank', 100], ['faster', 50],
            ['taunt', 'tank', 'tank', 'tank', 50]
        ]);
        waves.push([
            10, ['taunt', 'stronger', 'tank', 'stronger', 50],
            ['faster', 50]
        ]);
    }
    if (isWave(25)) {
        waves.push([5, ['taunt', 'medic', 'tank', 50], ['faster', 50]]);
        waves.push([5, ['taunt', 'faster', 'faster', 'faster', 50]]);
        waves.push([
            10, ['taunt', 'tank', 'tank', 'tank', 50],
            ['faster', 50]
        ]);
    }
    if (isWave(30)) {
        waves.push([5, ['taunt', 'faster', 'faster', 'faster', 50]]);
        waves.push([5, ['taunt', 'tank', 'tank', 'tank', 50]]);
        waves.push([5, ['taunt', 'medic', 'tank', 'tank', 50]]);
        waves.push([1, ['faster', 200]]);
    }
    if (isWave(35)) {
        waves.push([0, ['taunt', 'faster', 200]]);
    }

    return random(waves);
}

// Recalculate pathfinding maps
// Algorithm from https://www.redblobgames.com/pathfinding/tower-defense/
function recalculate() {
    walkMap = getWalkMap();
    var frontier = [];
    var target = vts(exit);
    frontier.push(target);
    var cameFrom = {};
    var distance = {};
    cameFrom[target] = null;
    distance[target] = 0;

    // Fill cameFrom and distance for every tile
    while (frontier.length !== 0) {
        var current = frontier.shift();
        var t = stv(current);
        var adj = neighbors(walkMap, t.x, t.y, true);

        for (var i = 0; i < adj.length; i++) {
            var next = adj[i];
            if (!(next in cameFrom) || !(next in distance)) {
                frontier.push(next);
                cameFrom[next] = current;
                distance[next] = distance[current] + 1;
            }
        }
    }

    // Generate usable maps
    dists = buildArray(cols, rows, null);
    var newPaths = buildArray(cols, rows, 0);
    var keys = Object.keys(cameFrom);
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var current = stv(key);

        // Distance map
        dists[current.x][current.y] = distance[key];

        // Generate path direction for every tile
        var val = cameFrom[key];
        if (val !== null) {
            // Subtract vectors to determine direction
            var next = stv(val);
            var dir = next.sub(current);
            // Fill tile with direction
            if (dir.x < 0) newPaths[current.x][current.y] = 1;
            if (dir.y < 0) newPaths[current.x][current.y] = 2;
            if (dir.x > 0) newPaths[current.x][current.y] = 3;
            if (dir.y > 0) newPaths[current.x][current.y] = 4;
        }
    }

    // Preserve old paths on path tiles
    for (var x = 0; x < cols; x++) {
        for (var y = 0; y < rows; y++) {
            if (grid[x][y] === 2) newPaths[x][y] = paths[x][y];
        }
    }

    paths = newPaths;
}

// Game start 
function resetGame() {
    loadMap();
    // Clear all entities
    enemies = [];
    projectiles = [];
    systems = [];
    towers = [];
    newEnemies = [];
    newProjectiles = [];
    newTowers = [];
    // Reset all stats
    health = 40;
    maxHealth = health;
    wave = 0;
    // Reset all flags
    paused = true;
    scd = 0;
    toCooldown = false;
    toPathfind = false;
    toPlace = false;
    // Start game
    nextWave();
}

// Changes tile size to fit everything onscreen
function resizeFit() {
    var div = document.getElementById('sketch-holder');
    var ts1 = floor(div.offsetWidth / cols);
    var ts2 = floor(div.offsetHeight / rows);
    ts = Math.min(ts1, ts2);
    resizeCanvas(cols * ts, rows * ts, true);
}

// Sell a tower
function sell(t) {
    selected = null;
    if (grid[t.gridPos.x][t.gridPos.y] === 0) toPathfind = true;
    clearInfo();
    cash += t.sellPrice();
    t.kill();
}

// Set a tower to place
function setPlace(t) {
    towerType = t;
    toPlace = true;
    updateInfo(createTower(0, 0, tower[towerType]));
}

// Visualize range of tower
function showRange(t, cx, cy) {
    stroke(255);
    fill(t.color[0], t.color[1], t.color[2], 63);
    var r = (t.range + 0.5) * ts * 2;
    ellipse(cx, cy, r, r);
}

// Display tower information
function updateInfo(t) {
    var name = document.getElementById('name');
    name.innerHTML = '<span style="color:rgb(' + t.color + ')">' + t.title +
    '</span>';
    document.getElementById('cost').innerHTML = 'Cost: $' + t.totalCost;
    document.getElementById('sellPrice').innerHTML = 'Sell price: $' +
    t.sellPrice();
    document.getElementById('upPrice').innerHTML = 'Upgrade price: ' +
    (t.upgrades.length > 0 ? '$' + t.upgrades[0].cost : 'N/A');
    document.getElementById('damage').innerHTML = 'Damage: ' + t.getDamage();
    document.getElementById('type').innerHTML = 'Type: ' +
    t.type.toUpperCase();
    document.getElementById('range').innerHTML = 'Range: ' + t.range;
    document.getElementById('cooldown').innerHTML = 'Avg. Cooldown: ' +
    t.getCooldown().toFixed(2) + 's';
    var buttons = document.getElementById('info-buttons');
    buttons.style.display = toPlace ? 'none' : 'flex';
    document.getElementById('info-div').style.display = 'block';
}

// Update pause button
function updatePause() {
    document.getElementById('pause').innerHTML = paused ? 'Start' : 'Pause';
}

// Update game status display with wave, health, and cash
function updateStatus() {
    document.getElementById('wave').innerHTML = 'Wave ' + wave;
    document.getElementById('health').innerHTML = 'Health: ' +
    health + '/' + maxHealth;
    document.getElementById('cash').innerHTML = '$' + cash;
}

// Upgrade tower
function upgrade(t) {
    if (cash >= t.cost) {
        cash -= t.cost;
        selected.upgrade(t);
        selected.upgrades = t.upgrades ? t.upgrades : [];
        updateInfo(selected);
    }
}

// Return whether tile is walkable: not a wall or tower
function walkable(col, row) {
    // Check if wall or tower-only tile
    if (grid[col][row] === 1 || grid[col][row] === 3) return false;
    // Check if tower
    if (getTower(col, row)) return false;
    return true;
}


// Main p5 functions

function preload() {
    loadSounds();
}

function setup() {
    var div = document.getElementById('sketch-holder');
    var canvas = createCanvas(div.offsetWidth, div.offsetHeight);
    canvas.parent('sketch-holder');
    resetGame();
}

function draw() {
    background(bg);

    // Update game status
    updatePause();
    updateStatus();

    // Update spawn and wave cooldown
    if (!paused) {
        if (scd > 0) scd--;
        if (wcd > 0 && toWait) wcd--;
    }

    // Draw basic tiles
    for (var x = 0; x < cols; x++) {
        for (var y = 0; y < rows; y++) {
            var t = tiles[display[x][y]];
            if (typeof t === 'function') {
                t(x, y, displayDir[x][y]);
            } else {
                stroke(border, borderAlpha);
                t ? fill(t) : noFill();
                rect(x * ts, y * ts, ts, ts);
            }
        }
    }

    // Draw spawnpoints
    for (var i = 0; i < spawnpoints.length; i++) {
        stroke(255);
        fill(0, 230, 64);
        var s = spawnpoints[i];
        rect(s.x * ts, s.y * ts, ts, ts);
    }

    // Draw exit
    stroke(255);
    fill(207, 0, 15);
    rect(exit.x * ts, exit.y * ts, ts, ts);

    // Spawn enemies
    if (canSpawn() && !paused) {
        // Spawn same enemy for each spawnpoint
        var name = newEnemies.shift();
        for (var i = 0; i < spawnpoints.length; i++) {
            var s = spawnpoints[i];
            var c = center(s.x, s.y);
            enemies.push(createEnemy(c.x, c.y, enemy[name]));
        }

        // Reset cooldown
        toCooldown = true;
    }

    // Update and draw enemies
    for (let i = enemies.length - 1; i >= 0; i--) {
        let e = enemies[i];
        // Update direction and position
        if (!paused) {
            e.steer();
            e.update();
            e.onTick();
        }
        // Kill if outside map
        if (outsideMap(e)) e.kill();
        // If at exit tile, kill and reduce player health
        if (atTileCenter(e.pos.x, e.pos.y, exit.x, exit.y)) e.onExit();
        // Draw
        e.draw();
        if (e.isDead()) enemies.splice(i, 1);
    }

    // Draw health bars
    if (healthBar) {
        for (var i = 0; i < enemies.length; i++) {
            enemies[i].drawHealth();
        }
    }

    // Update and draw towers
    for (let i = towers.length - 1; i >= 0; i--) {
        let t = towers[i];
        // Target enemies and update cooldowns
        if (!paused) {
            t.target(enemies);
            t.update();
        }
        // Kill if outside map
        if (outsideMap(t)) t.kill();
        // Draw
        t.draw();
        if (t.isDead()) towers.splice(i, 1);
    }

    // Update and draw particle systems
    for (let i = systems.length - 1; i >= 0; i--) {
        let ps = systems[i];
        ps.run();
        if (ps.isDead()) systems.splice(i, 1);
    }

    // Update and draw projectiles
    for (let i = projectiles.length - 1; i >= 0; i--) {
        let p = projectiles[i];

        if (!paused) {
            p.steer();
            p.update();
        }

        // Attack target
        if (p.reachedTarget()) p.explode()

        // Kill if outside map
        if (outsideMap(p)) p.kill();

        p.draw();

        if (p.isDead()) projectiles.splice(i, 1);
    }

    // Draw range of tower being placed
    if (doRange()) {
        var p = gridPos(mouseX, mouseY);
        var c = center(p.x, p.y);
        var t = createTower(0, 0, tower[towerType]);
        showRange(t, c.x, c.y);

        // Draw a red X if tower cannot be placed
        if (!canPlace(p.x, p.y)) {
            push();
            translate(c.x, c.y);
            rotate(PI / 4);

            // Draw a red X
            noStroke();
            fill(207, 0, 15);
            var edge = 0.1 * ts;
            var len = 0.9 * ts / 2;
            rect(-edge, len, edge * 2, -len * 2);
            rotate(PI / 2);
            rect(-edge, len, edge * 2, -len * 2);

            pop();
        }
    }

    // Show if towers are disabled
    if (stopFiring) {
        // Draw black rect under text
        noStroke();
        fill(0);
        rect(width - 60, 0, 60, 22);
        
        fill(255);
        text('Firing off', width - 55, 15);
    }

    projectiles = projectiles.concat(newProjectiles);
    towers = towers.concat(newTowers);
    newProjectiles = [];
    newTowers = [];

    // If player is dead, reset game
    if (health <= 0) {
        alert("Game Over! Your health has reached 0.");
        resetGame();
    }

    // Start next wave
    if (toWait && wcd === 0 || skipToNext && newEnemies.length === 0) {
        toWait = false;
        wcd = 0;
        nextWave();
    }

    // Wait for next wave
    if (noMoreEnemies() && !toWait) {
        wcd = waveCool;
        toWait = true;
    }

    // Reset spawn cooldown
    if (toCooldown) {
        scd = spawnCool;
        toCooldown = false;
    }

    // Recalculate pathfinding
    if (toPathfind) {
        recalculate();
        toPathfind = false;
    }
}

function mousePressed() {
    if (!mouseInMap()) return;
    var p = gridPos(mouseX, mouseY);
    var t = getTower(p.x, p.y);
    
    if (t) {
        // Clicked on tower
        selected = t;
        toPlace = false;
        updateInfo(selected);
    } else if (canPlace(p.x, p.y)) {
        buy(createTower(p.x, p.y, tower[towerType]));
    }
}
