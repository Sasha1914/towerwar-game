# TowerWar Game
**Description**
Bienvenue dans Tower War, un jeu de tower defense développé en utilisant JavaScript, HTML, CSS et la bibliothèque p5.js. Affrontez des vagues d'ennemis en plaçant stratégiquement des tours pour défendre votre base.
**Comment Jouer**
    
    Objectif : Empêchez les ennemis d'atteindre la sortie en plaçant des tours le long du chemin.
    Gestion des Ressources : Gagnez de l'argent en éliminant des ennemis pour acheter et améliorer des tours depuis le magasin.
    Défis Croissants : Affrontez des vagues d'ennemis de plus en plus difficiles à chaque niveau.

**Installation**

    Téléchargez les fichiers du jeu depuis ce dépôt.
    Placez les fichiers dans le répertoire de votre choix sur votre ordinateur.

Prévisualisation

    Assurez-vous d'avoir installé l'extension Live Preview de Visual Studio Code.
    Ouvrez le fichier index.html dans Visual Studio Code.
    Cliquez avec le bouton droit de la souris sur index.html.
    Sélectionnez "Open with Live Server" pour lancer la prévisualisation du jeu dans votre navigateur web par défaut.

**Technologies Utilisées**


    JavaScript
    HTML
    CSS
    p5.js (bibliothèque pour le rendu graphique)
